class apache::service ($service_name=$apache::params::service_name) 
{

#$service_name=$apache::params::service_name
service {  'start_apache':
           name    => $service_name,
           enable  => true,
           ensure  => running,
           require => Package['apache'],
# using notify to print the variable. however, once you add it , you will 
# get " No title provided " error. just hash notify once it prints the value

#           notify  => "variable value ${service_name}"


}


}
