## Apache Module - Multiple Virtual Hosts

- Requires Puppet-enterprise-3.8.1
- Expects CentOS/RHEL 6.x and Ubuntu 14.04.2 LTS   hosts


This is a very simple Apache Module  and could serve as a starting point for more
complex Apache projects. 
