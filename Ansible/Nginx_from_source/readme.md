## Installing Nginx from source

- Requires ansible 1.9.2
- Expects CentOS/RHEL 6.x hosts

These playbooks deploy a very basic implementation of Nginx.
It will will install nginx-1.8.0 in CentOS 6.6 

To use them, first edit the "hosts" inventory file and change the hostname under [web]
Then run the playbook, like this:

	ansible-playbook  site.yml

When the playbook run completes, you should be able to see Nginx 1.8.0 is installed in your webserver.

This is a very simple playbook and could serve as a starting point for more
complex Nginx projects. 
