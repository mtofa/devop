## Elasticsearch Logstash Kibana Deployment 

- Requires ansible 1.9.2
- Expects CentOS/RHEL 6.x hosts

These playbooks deploy a very basic implementation of ELK stack. 

To use them, first edit the "hosts" inventory file and change [elk_master] and [elk_client]

[elk_master]  - Master server with ELK stack

[elk_client] - CentOS/RHEL 6.x client servers


Also, edit the group_vars/all file to set the master server name.


Then run the playbook, like this:

	ansible-playbook  site.yml

When the playbook run completes, you should be able to see the Elasticsearch+Logstash+Kibana
running on master [elk_master] server and  Logstash-forwarder running on client servers [elk_client]

To access Kibana GUI
Go to : http://<masterservername>:5601/

This is a very simple playbook and could serve as a starting point for more
complex ELK projects. 

